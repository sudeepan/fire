(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      9726,        247]
NotebookOptionsPosition[      7888,        207]
NotebookOutlinePosition[      8276,        223]
CellTagsIndexPosition[      8233,        220]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData["Quit"], "Input",
 CellChangeTimes->{{3.8738633360037003`*^9, 3.873863339993722*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"4ad33447-6850-4dc9-b721-891919176fdf"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"SetDirectory", "[", 
  RowBox[{"NotebookDirectory", "[", "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.905872766213352*^9, 3.905872774648823*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"0c92e80a-1cf3-4c24-9c83-1ac701d657b4"],

Cell[BoxData["\<\"/home/mzeng/code/fire_public/FIRE6/examples\"\>"], "Output",
 CellChangeTimes->{3.9058727750849648`*^9},
 CellLabel->"Out[1]=",ExpressionUUID->"85ec3cff-7213-4565-998d-6efdaae98090"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Get", "[", 
   RowBox[{"FileNameJoin", "[", 
    RowBox[{"{", " ", 
     RowBox[{"\"\<..\>\"", ",", " ", "\"\<FIRE6.m\>\""}], "}"}], "]"}], "]"}],
   ";"}]], "Input",
 CellChangeTimes->{{3.873863025278846*^9, 3.873863030842923*^9}, {
   3.873863341404467*^9, 3.873863344354521*^9}, {3.879051708750394*^9, 
   3.87905171327861*^9}, {3.879052537687173*^9, 3.879052538248217*^9}, {
   3.879052951661418*^9, 3.8790529521146383`*^9}, {3.879086364818625*^9, 
   3.879086375210528*^9}, {3.9058721230197697`*^9, 3.905872154036002*^9}, 
   3.9058727787329197`*^9},
 CellLabel->"In[2]:=",ExpressionUUID->"72c3b0c1-01ce-40fd-bcba-150b63233ea5"],

Cell[BoxData["\<\"FIRE, version 6.4.dev\"\>"], "Print",
 CellChangeTimes->{3.905872246799*^9, 3.905872280991983*^9, 
  3.90587277906031*^9},
 CellLabel->
  "During evaluation of \
In[2]:=",ExpressionUUID->"356adf55-8a52-4fd9-acff-7f5c805cf84d"]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"p2", "=", "1"}], ";"}], " ", 
  RowBox[{"(*", " ", 
   RowBox[{
   "set", " ", "one", " ", "kinematic", " ", "variable", " ", "to", " ", 
    "1."}], " ", "*)"}]}]], "Input",
 CellChangeTimes->{{3.90587278526608*^9, 3.905872793559412*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"ea89bfb9-d686-4845-9b63-b26be2dcfa7e"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"Internal", "=", 
   RowBox[{"{", 
    RowBox[{"l1", ",", " ", "l2", ",", " ", "l3"}], "}"}]}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"External", "=", 
   RowBox[{"{", "p", "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Propagators", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"-", 
       RowBox[{"l1", "^", "2"}]}], "+", "m1sq"}], ",", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"l2", "^", "2"}]}], "+", "m2sq"}], ",", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{"l3", "^", "2"}]}], "+", "m3sq"}], ",", " ", 
     RowBox[{
      RowBox[{"-", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"p", "+", "l1", "+", "l2", "+", "l3"}], ")"}], "^", "2"}]}], 
      "+", "m4sq"}], ",", " ", 
     RowBox[{"p", "*", "l1"}], ",", " ", 
     RowBox[{"p", "*", "l2"}], ",", " ", 
     RowBox[{"p", "*", "l3"}], ",", " ", 
     RowBox[{"l1", "*", "l2"}], ",", " ", 
     RowBox[{"l2", "*", "l3"}]}], "}"}]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Replacements", "=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"p", "^", "2"}], "->", "p2"}], "}"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.874124309101882*^9, 3.874124366602744*^9}, {
   3.874124410131906*^9, 3.874124413283339*^9}, 3.874124914849025*^9, {
   3.879230874197794*^9, 3.87923088172495*^9}, {3.879230981518153*^9, 
   3.879231015605661*^9}, {3.8792473321218433`*^9, 3.879247337752132*^9}, {
   3.879592905369623*^9, 3.879592916409493*^9}, {3.879593067524588*^9, 
   3.8795930684053783`*^9}, 3.9058721136040897`*^9},
 CellLabel->"In[4]:=",ExpressionUUID->"5fe1d924-a4ce-45bd-9413-8c32a767bc97"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"PrepareIBP", "[", "]"}]], "Input",
 CellChangeTimes->{{3.873863276762391*^9, 3.873863279142654*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"66d162bd-c7fd-40db-91e8-a27e0e94a962"],

Cell[BoxData["\<\"Prepared\"\>"], "Print",
 CellChangeTimes->{3.873863287269224*^9, 3.873863350456368*^9, 
  3.873863507371499*^9, 3.873865480433591*^9, 3.8738656944753838`*^9, 
  3.874124554997669*^9, 3.874124930248579*^9, 3.879231029016802*^9, 
  3.8792311247309513`*^9, 3.8792473465136223`*^9, 3.879593082313139*^9, 
  3.879607043063966*^9, 3.9058722885189257`*^9, 3.9058728006537447`*^9},
 CellLabel->
  "During evaluation of \
In[8]:=",ExpressionUUID->"adfb157b-6619-4fb3-83a5-768f942f7c05"],

Cell[BoxData["True"], "Output",
 CellChangeTimes->{3.873863287288067*^9, 3.873863350528778*^9, 
  3.8738635073745613`*^9, 3.8738654804382877`*^9, 3.873865694483118*^9, 
  3.874124554999557*^9, 3.874124930251114*^9, 3.87923102901965*^9, 
  3.879231124734373*^9, 3.879247346529428*^9, 3.879593082315558*^9, 
  3.87960704306625*^9, 3.905872288570253*^9, 3.905872800662524*^9},
 CellLabel->"Out[8]=",ExpressionUUID->"052887ff-931b-4eb7-8134-ef9f34edbfa0"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Prepare", "[", 
   RowBox[{
    RowBox[{"AutoDetectRestrictions", "\[Rule]", "True"}], ",", 
    RowBox[{"LI", "\[Rule]", "True"}]}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.873863299553268*^9, 3.8738633252959223`*^9}, {
  3.90587230866722*^9, 3.905872314256259*^9}},
 CellLabel->"In[9]:=",ExpressionUUID->"012839a9-42c8-47b6-a794-08488cc0e36c"],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Dimension set to \"\>", "\[InvisibleSpace]", "9"}],
  SequenceForm["Dimension set to ", 9],
  Editable->False]], "Print",
 CellChangeTimes->{{3.873863300829625*^9, 3.873863351613639*^9}, 
   3.87386350938061*^9, 3.873865481478595*^9, 3.8738656958049393`*^9, 
   3.8741245566129303`*^9, 3.874124931418261*^9, 3.879231035023119*^9, 
   3.879231125499896*^9, 3.879247346733138*^9, 3.8795930891577387`*^9, 
   3.8796070440101*^9, 3.905872296676208*^9, 3.905872803646336*^9},
 CellLabel->
  "During evaluation of \
In[9]:=",ExpressionUUID->"f3abf983-039a-4f50-be5c-a44b09d6a969"],

Cell[BoxData["\<\"No symmetries\"\>"], "Print",
 CellChangeTimes->{{3.873863300829625*^9, 3.873863351613639*^9}, 
   3.87386350938061*^9, 3.873865481478595*^9, 3.8738656958049393`*^9, 
   3.8741245566129303`*^9, 3.874124931418261*^9, 3.879231035023119*^9, 
   3.879231125499896*^9, 3.879247346733138*^9, 3.8795930891577387`*^9, 
   3.8796070440101*^9, 3.905872296676208*^9, 3.9058728036623487`*^9},
 CellLabel->
  "During evaluation of \
In[9]:=",ExpressionUUID->"475a9e6d-c862-4bed-9b3d-868a4d2112ef"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"SaveStart", "[", 
   RowBox[{"\"\<bananaUnequal\>\"", ",", " ", "1"}], "]"}], ";"}]], "Input",
 CellChangeTimes->{{3.8738633623647137`*^9, 3.873863381690873*^9}, 
   3.874124597401957*^9, {3.87412492133644*^9, 3.874124921613935*^9}, {
   3.879593092859117*^9, 3.879593095075019*^9}},
 CellLabel->"In[8]:=",ExpressionUUID->"516dec5f-7fc5-41c0-93bb-db67403eaf22"],

Cell[BoxData["\<\"Saving initial data\"\>"], "Print",
 CellChangeTimes->{3.8738633822639236`*^9, 3.873863512405445*^9, 
  3.873865484426977*^9, 3.873865698444758*^9, 3.874124602278022*^9, 
  3.874124953538319*^9, 3.8792310586747837`*^9, 3.879231140411899*^9, 
  3.879247359667582*^9, 3.879593103043935*^9, 3.879607073124517*^9, 
  3.905872321441743*^9},
 CellLabel->
  "During evaluation of \
In[8]:=",ExpressionUUID->"af175e94-75ba-4bd1-aede-d2503fefcd7e"]
}, Open  ]]
},
WindowSize->{960, 492},
WindowMargins->{{0, Automatic}, {0, Automatic}},
FrontEndVersion->"13.1 for Linux x86 (64-bit) (June 16, 2022)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"b2159936-ab2f-42b6-9100-f8b8081e0b02"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 176, 2, 29, "Input",ExpressionUUID->"4ad33447-6850-4dc9-b721-891919176fdf"],
Cell[CellGroupData[{
Cell[759, 26, 248, 4, 29, "Input",ExpressionUUID->"0c92e80a-1cf3-4c24-9c83-1ac701d657b4"],
Cell[1010, 32, 200, 2, 33, "Output",ExpressionUUID->"85ec3cff-7213-4565-998d-6efdaae98090"]
}, Open  ]],
Cell[CellGroupData[{
Cell[1247, 39, 668, 13, 33, "Input",ExpressionUUID->"72c3b0c1-01ce-40fd-bcba-150b63233ea5"],
Cell[1918, 54, 244, 5, 25, "Print",ExpressionUUID->"356adf55-8a52-4fd9-acff-7f5c805cf84d"]
}, Open  ]],
Cell[2177, 62, 364, 9, 32, "Input",ExpressionUUID->"ea89bfb9-d686-4845-9b63-b26be2dcfa7e"],
Cell[2544, 73, 1667, 44, 120, "Input",ExpressionUUID->"5fe1d924-a4ce-45bd-9413-8c32a767bc97"],
Cell[CellGroupData[{
Cell[4236, 121, 202, 3, 29, "Input",ExpressionUUID->"66d162bd-c7fd-40db-91e8-a27e0e94a962"],
Cell[4441, 126, 496, 8, 23, "Print",ExpressionUUID->"adfb157b-6619-4fb3-83a5-768f942f7c05"],
Cell[4940, 136, 451, 6, 33, "Output",ExpressionUUID->"052887ff-931b-4eb7-8134-ef9f34edbfa0"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5428, 147, 386, 8, 29, "Input",ExpressionUUID->"012839a9-42c8-47b6-a794-08488cc0e36c"],
Cell[CellGroupData[{
Cell[5839, 159, 623, 12, 25, "Print",ExpressionUUID->"f3abf983-039a-4f50-be5c-a44b09d6a969"],
Cell[6465, 173, 502, 8, 25, "Print",ExpressionUUID->"475a9e6d-c862-4bed-9b3d-868a4d2112ef"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[7016, 187, 396, 7, 33, "Input",ExpressionUUID->"516dec5f-7fc5-41c0-93bb-db67403eaf22"],
Cell[7415, 196, 457, 8, 25, "Print",ExpressionUUID->"af175e94-75ba-4bd1-aede-d2503fefcd7e"]
}, Open  ]]
}
]
*)

