#!/bin/bash

echo "$*" | tr _ - > previous_options

echo "";
echo "This script is not a classical configure script.";
echo "It only creates the Makefile according to the options you pass.";
echo "Changes in some options require rebuilding of dependancy libraries (make cleandep && make dep) first.";
echo "The following options are valid:";
echo "";
echo "--enable_zlib: enables the zlib compressor shipped with kyotocabinet, requires the zlib/deflate library (zlib1g-dev)";
echo "--enable_snappy: tries to build the snappy library shipped with FIRE (might require some automake magic)";
echo "--enable_zstd: tries to build the ZStandard compressor";
echo "--enable_debug: add -g to compile options for debugging symbols";
echo "--enable_tcmalloc: use tcmalloc from google perf tools instead of the libc malloc"
echo "--enable_lthreads: enable multiple threads inside a sector (separated by levels): the #lthreads option in config"
echo "--disk_database: switch to the disk database mode instead of RAM database"
echo "--small_point: make point size 16 bytes instead of 24"
echo "--enable_symbolica: enable symbolica library for calculations without calls to external binary"
echo "--enable_flint: enable flint library for calculations without calls to external binary"
echo "--enable_prebuilt_gmp: use prebuilt GMP binaries that work for most current Linux distributions"
echo "--enable_prebuilt_mpfr: use prebuilt MPFR binaries that work for most current Linux distributions"
echo "-----------------------------------------------------------------------------------------------------------";
echo ""
echo "Now creating Makefile";

UNAME_S=$(uname -s)
if [ $UNAME_S = Darwin ]; then
  cpp=clang++
  cc=clang
  math=/Applications/Mathematica.app/Contents/MacOS/WolframKernel
else
  cpp=g++
  cc=gcc
  math=math
fi

enable_zlib=0
enable_snappy=0
enable_debug=0
enable_tcmalloc=0
enable_lthreads=0
enable_zstd=0
disk_database=0
small_point=0
enable_symbolica=0
enable_flint=0
prebuilt_gmp=0
prebuilt_mpfr=0
for var in "$@"; do
  case "$var" in
  "--enable-zlib") enable_zlib=1 ;;
  "--enable_zlib") enable_zlib=1 ;;
  "--enable-snappy") enable_snappy=1 ;;
  "--enable_snappy") enable_snappy=1 ;;
  "--enable-debug") enable_debug=1 ;;
  "--enable_debug") enable_debug=1 ;;
  "--enable-tcmalloc") enable_tcmalloc=1 ;;
  "--enable_tcmalloc") enable_tcmalloc=1 ;;
  "--enable-zstd") enable_zstd=1 ;;
  "--enable_zstd") enable_zstd=1 ;;
  "--small-point") small_point=1 ;;
  "--small_point") small_point=1 ;;
  "--enable-lthreads") enable_lthreads=1 ;;
  "--enable_lthreads") enable_lthreads=1 ;;
  "--disk_database") disk_database=1 ;;
  "--disk-database") disk_database=1 ;;
  "--enable-symbolica") enable_symbolica=1 ;;
  "--enable_symbolica") enable_symbolica=1 ;;
  "--enable-flint") enable_flint=1 ;;
  "--enable_flint") enable_flint=1 ;;
  "--enable_prebuilt_gmp") prebuilt_gmp=1 ;;
  "--enable_prebuilt_gmp") prebuilt_gmp=1 ;;
  "--enable_prebuilt_mpfr") prebuilt_mpfr=1 ;;
  "--enable_prebuilt_mpfr") prebuilt_mpfr=1 ;;
  *)    varstart="${var%=*}"
        varend="${var#*=}"
        case "$varstart" in
            "--cpp")
                cpp="$varend"
                ;;
            "--cc")
                cc="$varend"
                ;;
        *)
            echo "Option $var not recognized! Skipping it"
            ;;
        esac
    esac
done

paths=$(cat paths.inc.in)
text=`cat Makefile.in`
stext=`cat poly/Makefile.in`
ttext=`cat tools/Makefile.in`
ptext=`cat prime/Makefile.in`
mtext=`cat mpi/Makefile.in`
if [ $enable_zlib = 1 ]; then echo "Enabling zlib compressor";
    text=${text/"--disable-zlib "/""}; else
    stext=${stext/"-DWITH_ZLIB "/""};
    ptext=${ptext/"-DWITH_ZLIB "/""};
    stext=${stext/"-lz "/""};     stext=${stext/"-lz "/""};
    ptext=${ptext/"-lz "/""};    ptext=${ptext/"-lz "/""};
fi
if [ $small_point = 1 ]; then echo "Using small point size"; else
    stext=${stext/"-DSMALL_POINT"/""};
    ptext=${ptext/"-DSMALL_POINT"/""};
fi 
if [ $disk_database = 1 ]; then echo "Switching to disk database mode"; else
    stext=${stext/"-DDISK_DB"/""};
    ptext=${ptext/"-DDISK_DB"/""};
fi
if [ $enable_debug = 1 ]; then echo "Enabling debug symbols"; else
    stext=${stext/"-g "/""};
    ptext=${ptext/"-g "/""};
    mtext=${mtext/"-g "/""};
    ttext=${ttext/"-g "/""};
    stext=${stext/"-rdynamic "/""};
    ptext=${ptext/"-rdynamic "/""};
    ttext=${ttext/"-rdynamic "/""};
    mtext=${mtext/"-rdynamic "/""};
    stext=${stext/"-rdynamic "/""};
    ttext=${ttext/"-rdynamic "/""};
    ptext=${ptext/"-rdynamic "/""};
    mtext=${mtext/"-rdynamic "/""};
    ttext=${ttext/"-rdynamic "/""};
    stext=${stext/"-DWITH_DEBUG "/""};
    ptext=${ptext/"-DWITH_DEBUG "/""};
    mtext=${mtext/"-DWITH_DEBUG "/""};
    ttext=${ttext/"-DWITH_DEBUG "/""};
fi
if [ $enable_snappy = 1 ]; then echo "Enabling snappy compressor"; else
    stext=${stext/"-DWITH_SNAPPY "/""};
    stext=${stext//"-lsnappy "/""};
    ptext=${ptext/"-DWITH_SNAPPY "/""};
    ptext=${ptext//"-lsnappy "/""};
    paths=${paths/"ENABLE_SNAPPY=1"/""}
fi
if [ $enable_zstd = 1 ]; then echo "Enabling ZStandard compressor"; else
    stext=${stext/"-DWITH_ZSTD "/""};
    stext=${stext//"-lzstd "/""};
    ptext=${ptext/"-DWITH_ZSTD "/""};
    ptext=${ptext//"-lzstd "/""};
    paths=${paths/"ENABLE_ZSTD=1"/""}
fi
if [ $enable_tcmalloc = 1 ]; then echo "Enabling tcmalloc"; else
    stext=${stext/"-fno-builtin-malloc -fno-builtin-calloc -fno-builtin-realloc -fno-builtin-free"/""};
    stext=${stext//"-ltcmalloc_minimal "/""};
    ptext=${ptext/"-fno-builtin-malloc -fno-builtin-calloc -fno-builtin-realloc -fno-builtin-free"/""};
    ptext=${ptext//"-ltcmalloc_minimal "/""};
    paths=${paths/"ENABLE_TCMALLOC=1"/""}
fi
if [ $enable_lthreads = 1 ]; then
    echo "Enabling level threads";
    cp extra/kyotocabinet-1.2.77/kccachedb.h.threadsafe.in extra/kyotocabinet-1.2.77/kccachedb.h
    else
    cp extra/kyotocabinet-1.2.77/kccachedb.h.in extra/kyotocabinet-1.2.77/kccachedb.h
    stext=${stext/"-DFSBALLOCATOR_USE_THREAD_SAFE_LOCKING_PTHREAD "/""};
    ptext=${ptext/"-DFSBALLOCATOR_USE_THREAD_SAFE_LOCKING_PTHREAD "/""};
fi
if [ $enable_symbolica = 0 ]; then
  echo "Disabling symbolica lib"
  paths=${paths/"--enable-symbolica"/""}
  stext=${stext/"-lsymbolica "/""};
  stext=${stext/"-lsymbolica "/""};
  ptext=${ptext/"-lsymbolica "/""};
  ptext=${ptext/"-lsymbolica "/""};
  ttext=${ttext/"-lsymbolica "/""};
  ttext=${ttext/"-lsymbolica "/""};
fi
if [ $enable_flint = 0 ]; then
  echo "Disabling flint lib"
  paths=${paths/"--enable-flint"/""}
  stext=${stext/"-lflint "/""};
  stext=${stext/"-lmpfr -lflint "/""};
  ptext=${ptext/"-lflint "/""};
  ptext=${ptext/"-lmpfr -lflint "/""};
  ttext=${ttext/"-lflint "/""};
  ttext=${ttext/"-lmpfr -lflint "/""};
fi
if [ $prebuilt_gmp = 0 ]; then
  echo "Not using precompiled GMP binaries"
  paths=${paths/"--prebuilt-gmp"/""}
fi
if [ $prebuilt_mpfr = 0 ]; then
  echo "Not using precompiled MPFR binaries"
  paths=${paths/"--prebuilt-mpfr"/""}
fi

paths=${paths/"CPLUSPLUSCOMPILER"/"$cpp"}
paths=${paths/"CCOMPILER"/"$cc"}
paths=${paths/"MATHKERNEL"/"$math"}

echo "Using $cpp as the compiler for c++"
echo "Using $cc as the compiler for c"

echo "$paths" >paths.inc
echo "$text" > Makefile
echo "$stext" > poly/Makefile
echo "$ptext" > prime/Makefile
echo "$mtext" > mpi/Makefile
echo "$ttext" > tools/Makefile

echo "";
echo "Done";
echo "make dep: build dependancy libraries";
echo "make: build FIRE";
echo "";
echo "";
echo "Minimal gcc version required: 4.8.1"
echo "Current gcc version: $(gcc -dumpversion)"
echo "";
echo "";
