(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     34346,        890]
NotebookOptionsPosition[     32448,        824]
NotebookOutlinePosition[     32793,        839]
CellTagsIndexPosition[     32750,        836]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Three-loop on-shell propagator", "Title",
 CellChangeTimes->{{3.554301465671934*^9, 3.5543014667206793`*^9}, {
  3.5563005990081844`*^9, 3.5563006152061844`*^9}, {3.558332248952392*^9, 
  3.558332251307992*^9}}],

Cell[BoxData[{
 GraphicsBox[{
   {Dashing[{Medium, Medium}], 
    CircleBox[{-0.2935299756032391, 0.34809972167291353`}, {0.4242902739717298, 0.33784099794127287`},
      NCache[{0, Pi}, {0, 3.141592653589793}]]}, 
   {Dashing[{Medium, Medium}], 
    CircleBox[{0.2654729424599395, 0.3480997216729138}, {0.4242902739717298, 0.3378409979412724},
      NCache[{0, Pi}, {0, 3.141592653589793}]]}, 
   {AbsoluteThickness[3], 
    LineBox[{{-0.8724080219020536, 0.34809972167291214`}, {0.8811050331687764,
      0.3480997216729126}}]}, 
   PointBox[{0.6549906195834276, 0.3480997216729128}], 
   {AbsolutePointSize[6], PointBox[{0.6897632164316685, 0.3480997216729139}]}, 
   {AbsolutePointSize[6], 
    PointBox[{0.13076029836849046`, 0.34809972167291403`}]}, 
   {AbsolutePointSize[6], 
    PointBox[{-0.15881733151178978`, 0.3480997216729138}]}, 
   {AbsolutePointSize[6], 
    PointBox[{-0.7178202495749688, 0.34809972167291403`}]}, 
   StyleBox[InsetBox[
     StyleBox[
      TagBox["p",
       HoldForm],
      TextAlignment->Center,
      Background->GrayLevel[1.]], {-0.8724080219020537, 0.2785545279764308}, {
     Left, Baseline},
     Alignment->{Left, Top}],
    FontSize->12], InsetBox[
    StyleBox[
     TagBox["2",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {-0.588009319771533, 0.3220202740367331}, {
    Left, Baseline}, {0.034772596848241015`, 0.08693149212060254}, {{1., 
    0.}, {0., 1.}},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["3",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[
      1.]], {-0.31091627402735944`, 0.3220202740367324}, {Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["4",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[
      1.]], {-0.030424091214879656`, 0.3220202740367332}, {Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["5",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[
      1.]], {0.26547294245993935`, 0.32202027403673306`}, {Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["6",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.561040015669988, 0.3220202740367333}, {
    Left, Baseline},
    Alignment->{Left, Top}], 
   {Dashing[{Medium, Medium}], 
    CircleBox[{0.00434850563336131, 0.3480997216729132}, {0.4242902739717298, 0.33784099794127254`},
      NCache[{0, Pi}, {0, 3.141592653589793}]]}, 
   {AbsolutePointSize[6], PointBox[{0.4286387796050909, 0.348099721672913}]}, 
   {AbsolutePointSize[6], 
    PointBox[{-0.419941768338368, 0.34809972167291314`}]}, InsetBox[
    StyleBox[
     TagBox["7",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[
      1.]], {-0.31091627402735944`, 0.6598612719780063}, {
    Left, Baseline}, {0.03477259684824102, 0.08693149212060253}, {{1., 0.}, {
    0., 1.}},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["8",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[
      1.]], {-0.030424091214879767`, 0.6598612719780067}, {Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["9",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.28285924088406, 0.6598612719780069}, {
    Left, Baseline}, {0.034772596848241015`, 0.0869314921206025}, {{1., 0.}, {
    0., 1.}},
    Alignment->{Left, Top}]},
  ImagePadding->{{0., 1.}, {1., 0.}},
  ImageSize->{361., Automatic},
  PlotRange->{{-1.0417271162178892`, 
   1.0446286946765708`}, {-0.04265320334261856, 1.0400580315691736`}},
  PlotRangePadding->Automatic], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
    "The", " ", "first", " ", "entry", " ", "in", " ", "the", " ", "basis"}], 
    ",", " ", 
    RowBox[{"see", " ", "below"}], ",", " ", 
    RowBox[{"is", " ", "the", " ", "irreducible", " ", "numerator"}]}], 
   "*)"}]}]}], "Text",
 CellChangeTimes->{{3.5590397245408*^9, 3.559039964032*^9}, {
  3.5625397785399523`*^9, 3.562539826829714*^9}, {3.5641066561588564`*^9, 
  3.5641066743816833`*^9}}],

Cell[CellGroupData[{

Cell["Finding reduction rules", "Section",
 CellChangeTimes->{{3.5563006288211846`*^9, 3.5563006412801847`*^9}, 
   3.5563047894731846`*^9, {3.5563874491311193`*^9, 3.5563874694263287`*^9}, {
   3.556387654361615*^9, 3.556387654580011*^9}, {3.5581615194226*^9, 
   3.5581615214086*^9}, {3.5581950641949778`*^9, 3.5581950794205775`*^9}, 
   3.558332256112792*^9, {3.564230968437476*^9, 3.564230969248687*^9}}],

Cell[CellGroupData[{

Cell["Initialization", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239460051384*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"<<", "LiteRed`"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"SetDim", "[", "d", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Declare", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"l", ",", "r", ",", "k", ",", "p"}], "}"}], ",", "Vector"}], 
   "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"p", ",", "p"}], "]"}], "=", "1"}], 
  ";"}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, {
   3.5582393244405837`*^9, 3.558239326983384*^9}, {3.5584550621361914`*^9, 
   3.5584551101531916`*^9}, {3.562475854977724*^9, 3.5624758663293734`*^9}, {
   3.5624817980126467`*^9, 3.5624817980316477`*^9}, {3.562539938231086*^9, 
   3.5625399534019537`*^9}, {3.5641066342912245`*^9, 3.564106653009252*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Defining the basis and searching for the symmetries", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239495198184*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"NewBasis", "[", 
    RowBox[{"p3", ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"l", "-", "k"}], ",", 
       RowBox[{
        RowBox[{"sp", "[", 
         RowBox[{"p", "-", "l"}], "]"}], "-", "1"}], ",", 
       RowBox[{
        RowBox[{"sp", "[", 
         RowBox[{"p", "-", "l", "-", "r"}], "]"}], "-", "1"}], ",", 
       RowBox[{
        RowBox[{"sp", "[", 
         RowBox[{"p", "-", "l", "-", "r", "-", "k"}], "]"}], "-", "1"}], ",", 
       RowBox[{
        RowBox[{"sp", "[", 
         RowBox[{"p", "-", "r", "-", "k"}], "]"}], "-", "1"}], ",", 
       RowBox[{
        RowBox[{"sp", "[", 
         RowBox[{"p", "-", "k"}], "]"}], "-", "1"}], ",", "l", ",", "r", ",", 
       "k"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"l", ",", "r", ",", "k"}], "}"}], ",", 
     RowBox[{"Directory", "\[Rule]", "\"\<p3 dir\>\""}]}], "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"Basis", " ", 
    RowBox[{"definition", ".", " ", 
     RowBox[{"sp", "[", 
      RowBox[{"p", ",", "r"}], "]"}]}], " ", "is", " ", "the", " ", 
    "irreducible", " ", 
    RowBox[{"numerator", "."}]}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"GenerateIBP", "[", "p3", "]"}], "\n", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"IBP", " ", 
     RowBox[{"generation", ".", " ", "Can"}], " ", "be", " ", "also", " ", 
     "invoked", " ", "by", " ", "passing", " ", "GenerateIBP"}], "\[Rule]", 
    RowBox[{
    "True", " ", "option", " ", "in", " ", "NewBasis", " ", "call"}]}], 
   "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"AnalyzeSectors", "[", 
   RowBox[{"p3", ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "__"}], "}"}]}], "]"}], 
  RowBox[{"(*", 
   RowBox[{"Zero", " ", "and", " ", "simple", " ", "sectors", " ", 
    RowBox[{"determination", ".", " ", "The"}], " ", "second", " ", 
    "argument", " ", "is", " ", "a", " ", "pattern", " ", "which", " ", 
    "claims", " ", "that", " ", "the", " ", "sectors", " ", "with", " ", 
    RowBox[{"sp", "[", 
     RowBox[{"p", ",", "r"}], "]"}], " ", "in", " ", "the", " ", 
    "denominator", " ", "should", " ", "not", " ", "be", " ", "analyzed"}], 
   "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"FindSymmetries", "[", "p3", "]"}], 
  RowBox[{"(*", 
   RowBox[{"Finding", " ", "unique", " ", "and", " ", "mapped", " ", 
    RowBox[{"sectors", "."}]}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, 
   3.5582393244405837`*^9, 3.558239440582584*^9, {3.5583292212296*^9, 
   3.5583292265576*^9}, {3.56203042214354*^9, 3.562030435688315*^9}, {
   3.562032558348724*^9, 3.562032578402871*^9}, 3.5620334648835745`*^9, {
   3.562307902638079*^9, 3.5623079113865795`*^9}, {3.562310652406357*^9, 
   3.562310653363412*^9}, {3.562475808237051*^9, 3.5624758188366575`*^9}, {
   3.5641067161649456`*^9, 3.5641067230853567`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Searching for the reduction rules and saving", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.5582395181301837`*^9}, {
  3.5598844154207306`*^9, 3.5598844168278112`*^9}}],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{"SolvejSector", "/@", 
   RowBox[{"UniqueSectors", "[", "p3", "]"}]}], "]"}]], "Input"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"DiskSave", "[", "p3", "]"}], 
  RowBox[{"(*", 
   RowBox[{"Saving", "."}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Quit", "[", "]"}], 
  RowBox[{"(*", 
   RowBox[{"Quitting", " ", "the", " ", "kernel"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, 
   3.5582393244405837`*^9, 3.558239440582584*^9, {3.558247031471384*^9, 
   3.558247033518384*^9}, {3.5582581804033837`*^9, 3.5582581881233835`*^9}, 
   3.558259951983384*^9, 3.5584550557511916`*^9, {3.5594481473314185`*^9, 
   3.5594481475224295`*^9}, {3.5625400236849737`*^9, 3.5625400244060144`*^9}, 
   3.5641068121594915`*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Manual modifications", "Section",
 CellChangeTimes->{{3.5581950842253776`*^9, 3.558195085379778*^9}, {
  3.5594483000181513`*^9, 3.559448302655302*^9}, {3.564106820476305*^9, 
  3.5641068234947095`*^9}}],

Cell[CellGroupData[{

Cell["Initialization", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239460051384*^9}}],

Cell[BoxData["Quit"], "Input",
 CellChangeTimes->{{3.5641069768519354`*^9, 3.564106977471936*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"<<", "LiteRed`"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"Loading", " ", "the", " ", "package"}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"SetDirectory", "[", 
    RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}], "\n", 
  RowBox[{"(*", 
   RowBox[{"Setting", " ", "working", " ", "directory"}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"SetDim", "[", "d", "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{
   "d", " ", "stands", " ", "for", " ", "the", " ", "dimensionality"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Declare", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"l", ",", "r", ",", "k", ",", "p"}], "}"}], ",", "Vector"}], 
    "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"vector", " ", "variables"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"sp", "[", 
     RowBox[{"p", ",", "p"}], "]"}], "=", "1"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"on", "-", 
    RowBox[{"shellness", " ", "condition"}]}], 
   "*)"}]}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, {
   3.5582393244405837`*^9, 3.558239326983384*^9}, 3.558455073031192*^9, {
   3.5584551335091915`*^9, 3.5584551476541915`*^9}, {3.5641068277643166`*^9, 
   3.564106828809518*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"<<", "\"\<p3 dir/p3\>\""}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"Loading", " ", "the", " ", "basis"}], "*)"}]}]], "Input",
 CellChangeTimes->{{3.5581950872361774`*^9, 3.558195098982978*^9}, {
   3.5582393598369837`*^9, 3.558239417073384*^9}, {3.5582760384370003`*^9, 
   3.558276038749*^9}, {3.5584550455611916`*^9, 3.558455046696192*^9}, {
   3.5586776447539263`*^9, 3.558677645616976*^9}, 3.559448151590662*^9, {
   3.5594482358274803`*^9, 3.559448236739532*^9}, {3.5620689715916567`*^9, 
   3.5620689730112567`*^9}, 3.5641068389183326`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Graphs", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239460051384*^9}, {
  3.558239531858184*^9, 3.558239532950184*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"AttachGraph", "[", 
    RowBox[{
     RowBox[{"js", "[", 
      RowBox[{
      "p3", ",", "0", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", 
       "1", ",", "1", ",", "1"}], "]"}], ",", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"1", "\[Rule]", "2"}], ",", "\"\<1\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"2", "\[Rule]", "3"}], ",", "\"\<1\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"3", "\[Rule]", "4"}], ",", "\"\<1\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"4", "\[Rule]", "5"}], ",", "\"\<1\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"5", "\[Rule]", "6"}], ",", "\"\<1\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"1", "\[Rule]", "4"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"2", "\[Rule]", "5"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"3", "\[Rule]", "6"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"0", "\[Rule]", "1"}], ",", "\"\<p\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"6", "\[Rule]", "0"}], ",", "\"\<p\>\""}], "}"}]}], 
      "\[IndentingNewLine]", "}"}]}], "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{
   "Attach", " ", "the", " ", "graph", " ", "to", " ", "the", " ", "highest", 
    " ", "sector", " ", 
    RowBox[{"possible", ".", " ", "The"}], " ", "graphs", " ", "for", " ", 
    "the", " ", "subsectors", " ", "are", " ", "determined", " ", 
    RowBox[{"automatically", ".", " ", "The"}], " ", "external", " ", "legs", 
    " ", "should", " ", 
    RowBox[{"start", "/", "end"}], " ", "at", " ", "vertex", " ", "number", 
    " ", "0.", " ", "The", " ", "internal", " ", "vertices", " ", "should", 
    " ", "be", " ", "positive", " ", 
    RowBox[{"integer", ".", " ", "The"}], " ", "order", " ", "of", " ", "the",
     " ", "internal", " ", "lines", " ", "should", " ", "correspond", " ", 
    "to", " ", "the", " ", "order", " ", "of", " ", "denominators", " ", "in",
     " ", "the", " ", 
    RowBox[{"sector", "."}]}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"GraphPlot", ",", 
    RowBox[{"MultiedgeStyle", "\[Rule]", "0.3"}], ",", 
    RowBox[{"VertexRenderingFunction", "\[Rule]", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{"#2", ">", "0"}], ",", "Black", ",", "Blue"}], "]"}], ",", 
         RowBox[{"Disk", "[", 
          RowBox[{"#1", ",", "0.03"}], "]"}]}], "}"}], "&"}], ")"}]}], ",", 
    RowBox[{"EdgeRenderingFunction", "\[Rule]", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Replace", "[", 
        RowBox[{"#3", ",", 
         RowBox[{"{", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"\"\<1\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{"Thick", ",", "Black", ",", 
              RowBox[{"Line", "[", "#1", "]"}]}], "}"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"\"\<0\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{"Thin", ",", "Black", ",", "Dashed", ",", 
              RowBox[{"Line", "[", "#1", "]"}]}], "}"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"\"\<p\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"Thickness", "[", "0.01", "]"}], ",", "Blue", ",", 
              RowBox[{"Line", "[", "#1", "]"}]}], "}"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"_", "\[Rule]", 
            RowBox[{"Line", "[", "#1", "]"}]}]}], "\[IndentingNewLine]", 
          "}"}]}], "]"}], "&"}], ")"}]}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
   "Embellish", " ", "the", " ", "graph", " ", "output", " ", "by", " ", 
    "standard", " ", "Mathematica", " ", "means"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, 
   3.5578233848191853`*^9, {3.5581616433026*^9, 3.5581616492925997`*^9}, {
   3.5581617952995996`*^9, 3.5581618138736*^9}, {3.5581689496476*^9, 
   3.5581689894076*^9}, {3.5581729620156*^9, 3.5581729702226*^9}, 
   3.5581779450106*^9, {3.5582395409997835`*^9, 3.558239788622384*^9}, {
   3.5625372479972134`*^9, 3.562537251704425*^9}}],

Cell[BoxData[
 RowBox[{"GraphPlot", "/@", 
  RowBox[{"jGraph", "/@", 
   RowBox[{"MIs", "[", "p3", "]"}], 
   RowBox[{"(*", 
    RowBox[{"Draw", " ", "all", " ", "masters"}], "*)"}]}]}]], "Input",
 CellChangeTimes->{{3.5582399743113837`*^9, 3.558239984585384*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Manual modification of the reduction rules", "Subsubsection",
 CellChangeTimes->{{3.5582410319843836`*^9, 3.5582410501013837`*^9}}],

Cell["\<\
The integral j[p3,0,0,1,0,1,1,1,0,0] with the numerator sp[l,p] added is \
known to be expressible in terms of the integrals  j[p3,0,0,0,1,0,1,1,1,0] \
and j[p3,0,0,0,1,1,1,0,0,0], see,e.g., JHEP 1102 (2011) 102 [1010.1334].
So, we can manually take this into account and addd the rule for the \
elimination of the integral j[p3,0,0,2,0,1,1,1,0,0]:\
\>", "Text",
 CellChangeTimes->{{3.5582423340353837`*^9, 3.5582423440973835`*^9}, {
  3.5582425474323835`*^9, 3.5582426613923836`*^9}, {3.5582427323183837`*^9, 
  3.5582428174563837`*^9}, {3.562540063792268*^9, 3.562540093996995*^9}}],

Cell[BoxData[
 RowBox[{"jnum", "=", 
  RowBox[{"Toj", "[", 
   RowBox[{"p3", ",", 
    RowBox[{
     RowBox[{"j", "[", 
      RowBox[{
      "p3", ",", "0", ",", "0", ",", "1", ",", "0", ",", "1", ",", "1", ",", 
       "1", ",", "0", ",", "0"}], "]"}], 
     RowBox[{"sp", "[", 
      RowBox[{"l", ",", "p"}], "]"}]}]}], "]"}], 
  RowBox[{"(*", 
   RowBox[{
   "The", " ", "integral", " ", "with", " ", "the", " ", "numerator"}], 
   "*)"}]}]], "Input",
 CellChangeTimes->{{3.5582410947203836`*^9, 3.5582410949533834`*^9}, {
   3.558242256044384*^9, 3.5582422614373837`*^9}, {3.5582423053373837`*^9, 
   3.558242321926384*^9}, {3.558242826665384*^9, 3.558242852822384*^9}, {
   3.559890997274191*^9, 3.5598909987332745`*^9}, 3.562069105084857*^9, {
   3.562537314986045*^9, 3.562537317539191*^9}, {3.562538684356368*^9, 
   3.56253868595746*^9}, {3.5625395048782997`*^9, 3.562539506775408*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"{", 
    RowBox[{"{", "extrarule", "}"}], "}"}], "=", 
   RowBox[{"Solve", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"IBPReduce", "[", "jnum", "]"}], "\[Equal]", 
      RowBox[{
       RowBox[{
        RowBox[{"+", 
         FractionBox[
          RowBox[{
           RowBox[{"2", "d"}], "-", "5"}], 
          RowBox[{
           RowBox[{"2", "d"}], "-", "4"}]]}], 
        RowBox[{"j", "[", 
         RowBox[{
         "p3", ",", "0", ",", "0", ",", "0", ",", "1", ",", "0", ",", "1", 
          ",", "1", ",", "1", ",", "0"}], "]"}]}], "+", 
       RowBox[{
        FractionBox["1", "4"], 
        RowBox[{"j", "[", 
         RowBox[{
         "p3", ",", "0", ",", "0", ",", "0", ",", "1", ",", "1", ",", "1", 
          ",", "0", ",", "0", ",", "0"}], "]"}]}]}]}], ",", 
     RowBox[{"j", "[", 
      RowBox[{
      "p3", ",", "0", ",", "0", ",", "2", ",", "0", ",", "1", ",", "1", ",", 
       "1", ",", "0", ",", "0"}], "]"}]}], "]"}]}], 
  ";"}], "\[IndentingNewLine]", "extrarule"}], "Input",
 CellChangeTimes->{{3.558242909124384*^9, 3.558242946504384*^9}, {
  3.558243551058384*^9, 3.558243553306384*^9}, {3.562069119235057*^9, 
  3.5620691344294567`*^9}, {3.562069165224857*^9, 3.5620691676428566`*^9}, {
  3.562537343823694*^9, 3.5625373567274323`*^9}, {3.56253740529121*^9, 
  3.5625374230602264`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"p3", "/:", 
    RowBox[{"jRules", "[", 
     RowBox[{
     "p3", ",", "0", ",", "0", ",", "1", ",", "0", ",", "1", ",", "1", ",", 
      "1", ",", "0", ",", "0"}], "]"}], "=", 
    RowBox[{"Append", "[", 
     RowBox[{
      RowBox[{"jRules", "[", 
       RowBox[{
       "p3", ",", "0", ",", "0", ",", "1", ",", "0", ",", "1", ",", "1", ",", 
        "1", ",", "0", ",", "0"}], "]"}], ",", "extrarule"}], "]"}]}], ";"}], 
  RowBox[{"(*", 
   RowBox[{
   "Add", " ", "this", " ", "rule", " ", "to", " ", "the", " ", "rules", " ", 
    "for", " ", 
    RowBox[{"{", 
     RowBox[{
     "p3", ",", "0", ",", "1", ",", "0", ",", "1", ",", "1", ",", "1", ",", 
      "0", ",", "0", ",", "0"}], "}"}], " ", "sector"}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"MIs", "[", "p3", "]"}], "^=", 
    RowBox[{"DeleteCases", "[", 
     RowBox[{
      RowBox[{"MIs", "[", "p3", "]"}], ",", 
      RowBox[{"j", "[", 
       RowBox[{
       "p3", ",", "0", ",", "0", ",", "2", ",", "0", ",", "1", ",", "1", ",", 
        "1", ",", "0", ",", "0"}], "]"}]}], "]"}]}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"Also", " ", "remove", " ", 
    RowBox[{"j", "[", 
     RowBox[{
     "p3", ",", "0", ",", "1", ",", "0", ",", "1", ",", "2", ",", "1", ",", 
      "0", ",", "0", ",", "0"}], "]"}], " ", "from", " ", "the", " ", 
    "masters", " ", "list"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.5582429570543838`*^9, 3.558242959358384*^9}, {
  3.5582430006933837`*^9, 3.558243044867384*^9}, {3.558243187092384*^9, 
  3.558243204823384*^9}, {3.562069273567857*^9, 3.5620692963750567`*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"DiskSave", "[", "p3", "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"Save", " ", "to", " ", "disk"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{"Quit", "[", "]"}]}], "Input",
 CellChangeTimes->{{3.5582430552933836`*^9, 3.5582430745783834`*^9}, {
   3.5582432147063837`*^9, 3.5582432203113837`*^9}, {3.5582767200316*^9, 
   3.5582767260532*^9}, 3.5582791316798*^9, {3.5594481810103445`*^9, 
   3.559448185802619*^9}, {3.5594482669722614`*^9, 3.5594482683063374`*^9}, {
   3.562539668854678*^9, 3.5625396696747255`*^9}, {3.562540105157634*^9, 
   3.562540107561771*^9}, {3.564106865028369*^9, 3.564106865158369*^9}}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Reduction", "Section",
 CellChangeTimes->{{3.5581950842253776`*^9, 3.558195085379778*^9}, {
  3.5594483000181513`*^9, 3.559448310238736*^9}}],

Cell[CellGroupData[{

Cell["Reduction", "Subsubsection",
 CellChangeTimes->{{3.558240012205384*^9, 3.558240014085384*^9}}],

Cell[BoxData["Quit"], "Input",
 CellChangeTimes->{{3.563602359838499*^9, 3.5636023605093*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"SetDirectory", "[", 
    RowBox[{"NotebookDirectory", "[", "]"}], "]"}], ";"}], "\n", 
  RowBox[{"(*", 
   RowBox[{"Setting", " ", "working", " ", "directory"}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"<<", "LiteRed`"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"Loading", " ", "the", " ", "package"}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{
   RowBox[{"SetDim", "[", "d", "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{
   "d", " ", "stands", " ", "for", " ", "the", " ", "dimensionality"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"Declare", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"l", ",", "r", ",", "k", ",", "p"}], "}"}], ",", "Vector"}], 
    "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"vector", " ", "variables"}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"sp", "[", 
     RowBox[{"p", ",", "p"}], "]"}], "=", "1"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"on", "-", 
    RowBox[{"shellness", " ", "condition"}]}], 
   "*)"}]}], "\[IndentingNewLine]"}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, {
   3.5582393244405837`*^9, 3.558239326983384*^9}, 3.558455073031192*^9, {
   3.5584551335091915`*^9, 3.5584551476541915`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"<<", "\"\<p3 dir/p3\>\""}], ";"}]], "Input",
 CellChangeTimes->{{3.5582781914642*^9, 3.5582782068518*^9}, {
   3.558280129568*^9, 3.5582801298487997`*^9}, 3.5594481744029665`*^9, {
   3.562069403019657*^9, 3.5620694040648565`*^9}, 3.562540119443451*^9, {
   3.564107350467289*^9, 3.564107352147291*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Timing", "[", 
   RowBox[{"IBPReduce", "[", 
    RowBox[{
     RowBox[{"j", "[", 
      RowBox[{"p3", ",", 
       RowBox[{"-", "1"}], ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", 
       ",", "1", ",", "1", ",", "1"}], "]"}], ",", 
     RowBox[{"DWeight", "\[Rule]", "2"}]}], "]"}], "]"}], 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"The", " ", "reduction", " ", "of", " ", "some", " ", 
     RowBox[{
      RowBox[{"j", "[", "\[Ellipsis]", "]"}], ".", " ", "The"}], " ", "above",
      " ", "example", " ", "takes", " ", "1"}], "-", 
    RowBox[{"2", " ", 
     RowBox[{"min", "."}]}]}], "*)"}]}]], "Input",
 CellChangeTimes->{{3.558240021297384*^9, 3.558240035250384*^9}, {
  3.5582403187553835`*^9, 3.558240374935384*^9}, {3.558240935376384*^9, 
  3.5582409703853836`*^9}, {3.5582432494063835`*^9, 3.5582432495483837`*^9}, {
  3.558243698803384*^9, 3.558243702191384*^9}, {3.558243997346384*^9, 
  3.5582439978213835`*^9}, {3.562069412240257*^9, 3.562069417310257*^9}, {
  3.5635987187512465`*^9, 3.5635987243360567`*^9}, {3.5641074696078587`*^9, 
  3.56410747039546*^9}}],

Cell[BoxData[{
 RowBox[{"Timing", "[", 
  RowBox[{"IBPReduce", "[", 
   RowBox[{
    RowBox[{"LoweringDRR", "[", 
     RowBox[{
     "p3", ",", "0", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", 
      "1", ",", "1", ",", "1"}], "]"}], ",", "\"\<lDRR[011111111]\>\""}], 
   "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"takes", " ", "about", " ", "20", "min"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.5635993555915675`*^9, 3.563599355731968*^9}, {
  3.5641087650488963`*^9, 3.564108798433955*^9}, {3.564109897678486*^9, 
  3.5641099083957047`*^9}}],

Cell[BoxData[{
 RowBox[{"Timing", "[", 
  RowBox[{"IBPReduce", "[", 
   RowBox[{
    RowBox[{"RaisingDRR", "[", 
     RowBox[{
     "p3", ",", "0", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", 
      "1", ",", "1", ",", "1"}], "]"}], ",", "\"\<rDRR[011111111]\>\""}], 
   "]"}], "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"takes", " ", "about", " ", "20", "min"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.5582440428783836`*^9, 3.5582440627193837`*^9}, {
   3.5582441146333838`*^9, 3.5582441214253836`*^9}, {3.558277073124*^9, 
   3.5582770828584003`*^9}, {3.5582875428368*^9, 3.5582875529300003`*^9}, {
   3.562073540528657*^9, 3.5620735455986567`*^9}, {3.562074360670057*^9, 
   3.562074366348457*^9}, {3.562541590734604*^9, 3.562541601376212*^9}, {
   3.563598667754757*^9, 3.56359866957996*^9}, 3.564108808480373*^9, 
   3.5641099129509125`*^9}]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{707, 549},
WindowMargins->{{67, Automatic}, {46, Automatic}},
FrontEndVersion->"8.0 for Microsoft Windows (32-bit) (February 23, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 217, 3, 83, "Title"],
Cell[799, 27, 4532, 126, 236, "Text"],
Cell[CellGroupData[{
Cell[5356, 157, 408, 5, 71, "Section"],
Cell[CellGroupData[{
Cell[5789, 166, 105, 1, 27, "Subsubsection"],
Cell[5897, 169, 1662, 33, 132, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7596, 207, 142, 1, 27, "Subsubsection"],
Cell[7741, 210, 3551, 79, 232, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11329, 294, 190, 2, 27, "Subsubsection"],
Cell[11522, 298, 136, 3, 31, "Input"],
Cell[11661, 303, 1389, 24, 52, "Input"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[13099, 333, 209, 3, 71, "Section"],
Cell[CellGroupData[{
Cell[13333, 340, 105, 1, 27, "Subsubsection"],
Cell[13441, 343, 98, 1, 31, "Input"],
Cell[13542, 346, 2057, 50, 152, "Input"],
Cell[15602, 398, 590, 11, 31, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[16229, 414, 146, 2, 27, "Subsubsection"],
Cell[16378, 418, 5050, 121, 512, "Input"],
Cell[21431, 541, 264, 6, 31, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21732, 552, 137, 1, 27, "Subsubsection"],
Cell[21872, 555, 594, 9, 65, "Text"],
Cell[22469, 566, 895, 20, 52, "Input"],
Cell[23367, 588, 1368, 36, 125, "Input"],
Cell[24738, 626, 1637, 42, 112, "Input"],
Cell[26378, 670, 660, 12, 52, "Input"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[27087, 688, 147, 2, 71, "Section"],
Cell[CellGroupData[{
Cell[27259, 694, 100, 1, 27, "Subsubsection"],
Cell[27362, 697, 94, 1, 31, "Input"],
Cell[27459, 700, 2005, 49, 152, "Input"],
Cell[29467, 751, 338, 6, 31, "Input"],
Cell[29808, 759, 1115, 24, 52, "Input"],
Cell[30926, 785, 592, 14, 52, "Input"],
Cell[31521, 801, 887, 18, 52, "Input"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

