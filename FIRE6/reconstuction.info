Rational reconstruction approach with 2 variables (d and x):

0) Run a simpler original task so that you end in a proper basis of master-integrals.
All the denomimators are factored out into factors of d and separate factors of x.

Then you are going to run your task with mpi to have multiple tables named TASK-d-x-p.tables
This is achieved with the bin/FIRE6_MPI binary (see details in mpi/README)
d here stands for the value of d (for example 100...)
x here stands for the value of x (for example 100...)
p here stands for the big prime number (for example 1...)

1) Fix a value of d and a value of x and create a number of tables. So that you can do the rational reconstruction

RationalReconstructTables[
  "TASK-" <> ToString[d0] <> "-" <> ToString[x0] <> ".tables", p0, True]

p0 stands for the number of different big primes in tables (1 <= pnum <= p0)
True stands for silent, gives only summary for each table reconstruction.
If you encounter "Rational reconstruction unstable" increase p0.

After that you know the number of prime numbers you need for the reconstruction. Add 1 or 2 to it to be safe.

2) Fix a value of x and create a number of tables with different values of d and p
untill you can succeed with Thiele reconstruction over d

For[x = 100, x <= x0, ++x,
  RationalReconstructTables[
    "TASK-" <> ToString[d0] <> "-" <> ToString[x] <> ".tables", p0, True]
];
ThieleReconstructTables["TASK-100.tables", Range[100, x0], True]

First 100 is the chosen d number,
x0 is the maximal x0 for which you have tables.
True stands for the silent mode (only summary)

If the reconstruction is unstable, increase x0;

When the Thiele reconstruction is stable, recover the required factor from new tables:
DenominatorFactor["TASK-100.tables"] /. {d->x}

Note: the replacement here is needed because the Thiele reconstruction originally is intended for
the case of one variables and assumes it is d.

The answer returned is the worst denominator and will wor as you factor in next steps.
To be safe you can recheck the factor with another d value reconstruction.

Note: Thiele reconstruction in x needs more values that the later coming Newton reconstruction
after multiplying by a coefficient.

So when you have the factor rerun the reconstruction with

NewtonThieleReconstructTables["TASK.tables", Range[dd, dd], Range[100, x0], x, factor, True]

The answer in definitely Thiele-unstable, but it has to be Newton-stable by x.
From this you know the real x0 you need later (it is smaller)

3) Now create all the needed tables.

Run integer reconstruction in Mathematica, for example with

For[d0 = 100, d0 <= ???, ++d0,
 For[x0 = 100, x0 <= ???, ++x0,
  RationalReconstructTables[
    "TASK-" <> ToString[d0] <> "-" <> ToString[x0] <> ".tables", p0, True]
  ]
]

To be safe, recheck that the Newton reconstruction is stable for other values of d
You can do it with

For[dd=100,dd!=d0,++dd,
    Print[Timing[NewtonThieleReconstructTables["TASK.tables", Range[dd, dd], Range[100, x0], x, factor, True]]];
];
x0 stands for the maximum x you have tables for
factor is the multiplication coefficient
True stands for silent mode (only summary for each run)

If everything is ok, you can proceed

6) Run the final NT reconstruction with

NewtonThieleReconstructTables["TASK.tables", Range[100, d0], Range[100, x0], x, factor, False]

d0 and x0 are the maximal numbers where you have tables, factor is the multiplication coefficient
False stands for not having silent mode so that you do not wait forever and start seing as the coefficients proceed

If you see "Thiele-unstable" messages, again go back and construct more tables.

If it stable, you are done!
